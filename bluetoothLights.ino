/*
 * File: bluetoothLights.ino
 * Author: Mcveigth Ojeda
 * Date: 2023-12-28
 * Brief: This program interfaces with a Bluetooth module (HC-05) to receive signals and control an RGB LED strip. The code parses incoming Bluetooth data and translates it into commands to adjust the color and brightness of the LED strip. It establishes a communication link with the Bluetooth module and translates the received commands into PWN signals to control the RED, GREEN, and BLUE channels of the RGB LED strip.
 */
#include <SoftwareSerial.h>

SoftwareSerial myBT(10, 11); // pin 10 as RX, pin 11 as TX

String Data = "";  // variable to store received character from myBT

// Define pins for RGB
#define RED 6
#define GREEN 5
#define BLUE 3

// Define minimum and maximum values for light input channels
const int RED_MIN = 0;
const int RED_MAX = 255;
const int GREEN_MIN = 256;
const int GREEN_MAX = 511;
const int BLUE_MIN = 512;
const int BLUE_MAX = 767;

void setup() {
	Serial.begin(9600);
	myBT.begin(38400);
	pinMode(RED, OUTPUT);
	pinMode(GREEN, OUTPUT);
	pinMode(BLUE, OUTPUT);
}

void loop() {
	while (myBT.available()) {
		char character = myBT.read();
		Data.concat(character);

		if (character == '\n') {
			Serial.write("Received: ");
			Serial.println(Data);
			int parsedValue = Data.toInt();

			if(parsedValue >= 0 && parsedValue <= 255) {
				analogWrite(RED, parsedValue);
				Serial.write("RED ON");
			}

			if(parsedValue >= 256 && parsedValue <= 511) {
				parsedValue -= GREEN_MIN;
				Serial.write(parsedValue);
				analogWrite(GREEN, parsedValue);
				Serial.write("GREEN ON");
			}

			if(parsedValue >= 512 && parsedValue <= 767) {
				parsedValue -= BLUE_MIN;
				analogWrite(BLUE, parsedValue);
				Serial.write("BLUE ON");
			}

			Data = "";
		}
	}
}
